########################################
### Configuration
########################################
# - Project options - #
option(ENABLE_TEST  "Enable building test units" ON)

########################################
### General Setup
########################################
project(pong)                        # Set ${PROJECT_NAME} poperty
cmake_minimum_required(VERSION 2.8)

# - CMake properties - #
#include(cmake/utils.cmake)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")   # Path of 'shared' lib
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")   # Path of 'static' lib
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")   # Path of 'exec' file
set(RUNTIME_TESTS_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/tests") # Path of `test exec' file
set(CMAKE_INSTALL_PREFIX           "/usr/local")                # Path of 'install' prefix
set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)

set(CMAKE_C_COMPILER      "gcc")
set(CMAKE_C_FLAGS         "")
set(CMAKE_C_FLAGS_DEBUG   "")
set(CMAKE_C_FLAGS_RELEASE "")

set(CMAKE_CXX_COMPILER      "g++")
set(CMAKE_CXX_FLAGS         "")
set(CMAKE_CXX_FLAGS_DEBUG   "")
set(CMAKE_CXX_FLAGS_RELEASE "")

set(CMAKE_AR     "gcc-ar")
set(CMAKE_RANLIB "gcc-ranlib")
set(CNAKE_NM     "gcc-nm")

########################################
### Pre-Build
########################################
# - Include Folders - #
include_directories("${CMAKE_SOURCE_DIR}/include")
#header_directories()
#header_merge()
#directory_namespace()

# - Source Files - #
file(GLOB_RECURSE SOURCE_FILES "${CMAKE_SOURCE_DIR}/src/*.*")

# - Header Files - #
file(GLOB_RECURSE HEADER_FILES "${CMAKE_SOURCE_DIR}/src/*.*")

# - Library Folders - #
link_directories("${CMAKE_BINARY_DIR}/lib")

# - Compiler options - #
set(C_COMMOM     "-std=c11")
set(CXX_COMMON   "-std=c++17 -fno-pretty-templates")
set(C_CXX_COMMON "-Wall -Wextra -pipe")

set(LTO "-flto -flto-compression-level=0 -flto-partition=none -flto-odr-type-merging -fuse-linker-plugin -ffat-lto-objects")
set(PDO "-fprofile-use")
set(OPTIMIZE_OPTIONS "${LTO} ${PDO} -O3 -march=native -mtune=native")

set(DEBUG_OPTIONS "-g3 -gdwarf-4 -p -pg -fno-inline")

set(CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG}   ${C_COMMOM} ${C_CXX_COMMON} ${DEBUG_OPTIONS}")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${C_COMMOM} ${C_CXX_COMMON} ${OPTIMIZE_OPTIONS}")

set(CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG}   ${CXX_COMMON} ${C_CXX_COMMON} ${DEBUG_OPTIONS}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${CXX_COMMON} ${C_CXX_COMMON} ${OPTIMIZE_OPTIONS}")

########################################
### Build
########################################
# - Build As Executable - #
add_executable(${PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES})

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/module" ${CMAKE_MODULE_PATH})

# - Link Against Libraries - #
# SFML
#find_package(SFML 2 REQUIRED system window graphics)
#target_link_libraries(${PROJECT_NAME} ${SFML_LIBRARIES})
target_link_libraries(${PROJECT_NAME} "sfml-system" "sfml-window" "sfml-graphics")

# OpenGL
#find_package(OpenGL REQUIRED)
#include_directories(${OPENGL_INCLUDE_DIR})
#target_link_libraries(${PROJECT_NAME} ${OPENGL_LIBRARIES})
target_link_libraries(${PROJECT_NAME} "GL")

########################################
### Install
########################################
#install(DIRECTORY   "${CMAKE_SOURCE_DIR}/include/<lib-name>/"
#        DESTINATION "include/<lib-name>"
#        FILES_MATCHING PATTERN "*.h*")

#install(TARGETS ${PROJECT_NAME} DESTINATION lib)

########################################
### Test units
########################################
if (ENABLE_TEST)
    # - Test Units - #
    # - Benchmark Units - #
endif()
