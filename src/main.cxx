////////////////////////////////////////////////////////////////////////////////
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////
/// \brief Entry functions.
/// \param argc Number of arguments.
/// \param argv Arguments passed to program.
/// \return error code.
////////////////////////////////////////
int main()
{
    sf::Window window(sf::VideoMode(800, 600), "Pong Game", sf::Style::Titlebar | sf::Style::Close);
    sf::Event  event;

    // main loop.
    while (window.isOpen()) {
        // event handler.
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // clear buffers.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // ... draw ...

        // swaps the front and back buffers.
        window.display();
    }

    return 0;
}
